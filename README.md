# Generate Contentful Model

A CLI application that creates an equivalent Terraform resource from a Contentful model with a given name and located by its space and environment IDs.

```
generate-contentful-model --spaceId [spaceId] --envId [envId] --token [management_token] --modelName [modelName]
```

## Example
```bash
➜ node ./dist/index.js --spaceId e0jts45wbzv2 --envId master --token $CONTENTFUL_MANAGEMENT_TOKEN --modelName CodeSnippet 

resource "contentful_contenttype" "CodeSnippet" {
    name = "Code Snippet"
    description = "A formatted code snippet with an optional title, alt-text, style directive, and code."
    display_field = "title"
    content_type_id = "CodeSnippet"
    space_id   = "e0jts45wbzv2"
    env_id     = "master"
    depends_on = [contentful_environment.master]
    
    field {
        name = "Title"
        id   = "title"
        type = "Symbol"
        required = "true"
        disabled = "false"
        omitted  = "false"
        
        validations = [
            jsonencode([{"unique":true}])
        ]
    }

    field {
        name = "Description"
        id   = "description"
        type = "Symbol"
        required = "true"
        disabled = "false"
        omitted  = "false"
        
    }

    field {
        name = "Style"
        id   = "style"
        type = "Symbol"
        required = "true"
        disabled = "false"
        omitted  = "false"
        
        validations = [
            jsonencode([{"in":["bash","css","dockerfile","go","graphql","hcl","html","js","json","jsx","makefile","mermaid","markdown","rust","text","tsx","ts","yaml","yml"],"message":""}])
        ]
    }

    field {
        name = "Code"
        id   = "code"
        type = "Text"
        required = "true"
        disabled = "false"
        omitted  = "false"
        
    }

    field {
        name = "Caption"
        id   = "caption"
        type = "Text"
        required = "true"
        disabled = "false"
        omitted  = "false"
        
    }
}
```