#!/usr/bin/env node

import terraformResourceTemplate from './terraformResourceTemplate'
import getModel from './getModel'

const getArgs = () => {
    const argsArray = []
    const args = process.argv.slice(2)
    const chunkSize = 2;
    for (let i = 0; i < args.length; i += chunkSize) {
        const chunk = args.slice(i, i + chunkSize);
        argsArray.push([chunk[0].replace('--', ''), chunk[1]])
    }
    return Object.fromEntries(argsArray)
}

const args = getArgs()
const {spaceId, envId, modelName, token} = args

if (
    spaceId
    && envId
    && token
    && modelName
) {
    getModel(
        spaceId,
        envId,
        modelName,
        token
    ).then(modelData =>
        console.log(terraformResourceTemplate(modelData, spaceId, envId))
    )
  } else {
    console.log('generate-tf-model --spaceId [spaceId] --envId [envId] --token [management_token] --modelName [modelName]')
  }
