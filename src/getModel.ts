const fetchContentModels = async (spaceId: string, envId: string, token: string) => {
    const response = await fetch(
        `https://api.contentful.com/spaces/${spaceId}/environments/${envId}/content_types`,
        {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
    )

    return response.json()
}

const getModel = async (spaceId: string, envId: string, modelName: string, token: string) => {
    const data = await fetchContentModels(spaceId,envId, token) as any
    // find the object in items[].sys.id === modelName
    const result = data.items.filter( model => model.sys.id === modelName )
    return result[0] || {}
}

export default getModel
