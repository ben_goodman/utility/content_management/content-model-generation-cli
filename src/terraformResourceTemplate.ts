const terraformFieldTemplate = (field: any) => (
    `
    field {
        name = "${field.name}"
        id   = "${field.id}"
        type = "${field.type}"
        required = "${field.required || true}"
        disabled = "${field.disabled || false}"
        omitted  = "${field.omitted || false}"
        ${field.validations.length > 0 ? `
        validations = [
            jsonencode(${JSON.stringify(field.validations) || {}})
        ]`
        : ''
    }
    }`
)

const terraformResourceTemplate = (modelJson: any, spaceId: string, envId: string) => (
    `resource "contentful_contenttype" "${modelJson.sys.id}" {
    name = "${modelJson.name}"
    description = "${modelJson.description}"
    display_field = "${modelJson.displayField}"
    content_type_id = "${modelJson.sys.id}"
    space_id   = "${spaceId}"
    env_id     = "${envId}"
    depends_on = [contentful_environment.master]
    ${modelJson.fields.map(terraformFieldTemplate).join('\n')}
}`
)

export default terraformResourceTemplate
